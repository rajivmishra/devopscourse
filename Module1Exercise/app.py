from flask import Flask, render_template,url_for, request, redirect
from Module1Exercise.flask_config import Config
from Module1Exercise.data.session_items import get_items, add_item, save_item, get_item
import os


app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY')
# Get all saved TODO items in the session
@app.route('/')
def index():

   todo_items = get_items()
   return render_template("index.html", todo_items = todo_items)

# Add the TODO item in the session
@app.route('/addItem', methods=['GET', 'POST'])
def addItem():
   titles=request.form.get('title_name')
   add_item(titles)
   return redirect('/')


if __name__ == '__main__':
   app.run(debug = True)
